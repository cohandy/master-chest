<?php
if(isset($_GET['p'])) {
	if($_GET['p'] === "error") {
		$page_title = "Master Chest | Error";
	} else {
		$url = $_SERVER['REQUEST_URI'];
		$split = explode("/", $url);
		$split_name = explode("-", $split[count($split) - 1]);
		$name = ucwords(implode(" ", $split_name));
		$page_title = "Master Chest | $name";
	}
} else {
	$page_title = "Master Chest | Home";
}
echo "<title>$page_title</title>";
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="assets/css/min-all.css">
<link rel="icon" type="image/png" href="assets/images/fire-favicon.png">
<script src="assets/js/all.js"></script>