<div class="main-container">
	<div id="error-page-header">
		<h3>Woops! Looks like the page you're looking for isn't actually there.</h3>
		<p>Try going back to the home page</p>
		<a href="/" class="theme-button">Home</a>
	</div>
</div>