<?php
//get trending games
$trendingGamesRequest = Game::topTrendingGames("hypes");
!isset($trendingGamesRequest->raw_body) ? $trendingGames = "false" : $trendingGames = $trendingGamesRequest->raw_body;
//get news stories
$pulsesRequest = Game::getPulses();
!isset($pulsesRequest->raw_body) ? $pulses = "false" : $pulses = $pulsesRequest->raw_body;
?>
<!-- holds all objects returned from igdb requests -->
<script>
var apiObjects = function() {
	var trendingGames = <?php echo $trendingGames; ?>;
	var pulses = <?php echo $pulses; ?>;
	var upcomingGames = null;
	function getTrendingGames() {
		return trendingGames;
	}
	function getPulses() {
		return pulses;
	}
	function setUpcomingGames(arr) {
		upcomingGames = arr;
	}
	function getUpcomingGames() {
		return upcomingGames;
	}
	return {
		trendingGames: getTrendingGames,
		pulses: getPulses,
		upcomingGames: getUpcomingGames,
		setUpcomingGames: setUpcomingGames
	}
}();
</script>
<!-- main page js -->
<script src="assets/js/home.js"></script>
<!-- quick nav when searching -->
<div id="home-nav">
	<div class="home-nav-half" id="back-to-menu" title="Back to home">
		<span class="flaticon-back"></span>
	</div>
	<div class="home-nav-half" title="Top of page">
		<span class="flaticon-up-arrow"></span>
	</div>
</div>
<!-- start of body html -->
<div id="home-page-banner">
	<div id="home-banner-transition">
		<div class="main-container">
			<h2 class="game-row-header header-tag" id="in-transition-header">Trending Games</h2>
		</div>
	</div>
	<div id="home-banner-cover"></div>
	<div class="main-container" id="home-banner-search">
		<img src="assets/images/home-graphic-transparent.png" id="home-main-pic">
		<form method="post" action="#" id="search-form">
			<div id="search-icon" class="flaticon-map"></div>
			<input type="text" name="q" id="search-input" placeholder="Search for your favorite games...">
		</form>
	</div>
</div>
<!-- search container -->
<div class="main-container" id="search-home-container">
	<div id="search-results"></div>
	<img src="assets/images/ball-triangle.svg" class="search-loading-svg" id="search-loading"/>
	<div id="show-more" class="theme-button">SHOW MORE</div>
</div>
<!-- main page container -->
<div class="main-container" id="main-home-container">
	<!-- trending games -->
	<div class="home-game-row" id="trending-games">
		<div class="slideshow-icon-div slide-right"><span class="flaticon-next slideshow-flaticon"></span></div>
		<div class="slideshow-icon-div slide-left"><span class="flaticon-back slideshow-flaticon"></span></div>
		<div class="game-card-holder">
		</div>
	</div>
	<!-- news stories -->
	<div id="news-container">
		<img src="assets/images/ball-triangle.svg" id="news-loader"/>
		<h2 class="header-tag">News From Around The Web</h2>
		<div class="flex-wrapper top-flex">
			<div class="flex-item flex-main animation-element"></div>
			<div class="flex-item flex-sub-main animation-element"></div>
		</div>
		<div class="flex-wrapper middle-flex">
			<div class="flex-item flex-square animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
		</div>
		<div class="flex-wrapper bottom-flex">
			<div class="flex-item flex-small animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
		</div>
		<div class="flex-wrapper bottom-flex" id="small-news-tiles">
			<div class="flex-item flex-small animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
			<div class="flex-item flex-small animation-element"></div>
		</div>
	</div>
	<!-- upcoming games -->
	<h2 class="header-tag">Upcoming Games</h2>
	<div class="home-game-row" id="upcoming-games">
		<div class="slideshow-icon-div slide-right"><span class="flaticon-next slideshow-flaticon"></span></div>
		<div class="slideshow-icon-div slide-left"><span class="flaticon-back slideshow-flaticon"></span></div>
		<div class="game-card-holder">
			<img src="assets/images/ball-triangle.svg" class="loading-svg"/>
		</div>
	</div>
</div>
