<?php
if(isset($_GET['id'])) {
	$gameRequest = Game::getGame($_GET['id']);
	if(isset($gameRequest->raw_body)) {
		$gameJS = $gameRequest->raw_body;
		$game = json_decode($gameRequest->raw_body)[0];
	} else {
		$gameJS = "false";
		$game = false;
	}
}
//if name is set then game was found, if not show error page
if(isset($game->name)) { 
//fetch game developer
$game_company = Game::getGameCompany($game->developers[0]);
isset($game_company->raw_body) ? $game_company_js = $game_company->raw_body : $game_company_js = "false";
//shorten game name if needed
if(strlen($game->name) > 36) {
	$game_name = substr($game->name, 0, 36) . "...";
} else $game_name = $game->name;
//check if cover
if(isset($game->cover)) {
	$cover = "//images.igdb.com/igdb/image/upload/t_cover_big/" . $game->cover->cloudinary_id;
	$thumb_cover = "//images.igdb.com/igdb/image/upload/t_thumb/" . $game->cover->cloudinary_id;
} else {
	$cover = "assets/images/chest.jpg";
	$thumb_cover = "assets/images/chest.jpg";
}
//check if screenshots
if(isset($game->screenshots)) {
	$header_img = "//images.igdb.com/igdb/image/upload/t_screenshot_big/" . $game->screenshots[mt_rand(0, count($game->screenshots) - 1)]->cloudinary_id;
} else {
	$header_img = "assets/images/home-header-bckg.jpg";
}
?>
<!-- holds ref to game object returned by igdb api and all its attributes -->
<script>
var gameObj = function() {
	var game = <?php echo $gameJS; ?>;
	var gameCompany = <?php echo $game_company_js; ?>;
	function getGame() {
		return game[0];
	}
	function getGameCompany() {
		if(gameCompany[0].hasOwnProperty('name')) {
			return gameCompany[0].name;
		} else return false;
	}
	return {
		game: getGame,
		company: getGameCompany
	}
}();
</script>
<!-- game show page js -->
<script src="assets/js/games/show.js"></script>
<!-- navbar -->
<?php if(isset($game->screenshots) && isset($game->videos)) { ?>
<div class="navbar" id="quick-navbar">
	<span id="nav-info" class="quick-nav-span">info</span>
	<span id="nav-video" class="quick-nav-span">videos</span>
	<span id="nav-screenshots" class="quick-nav-span">screenshots</span>
</div>
<div class="navbar" id="main-navbar">
	<img src=<?php echo $thumb_cover; ?> id="nav-cover-thumb">
</div>
<?php } //end of quick nav if ?>
<!-- main html -->
<div id="show-header">
	<div class="show-header-overlay"></div>
	<?php if(isset($game->screenshots)) { ?>
		<img src=<?php echo $header_img; ?> class="home-card-pic"/>
	<?php } else { ?>
		<img src="assets/images/home-header-bckg.jpg" class="home-card-pic"/>
	<?php } //end of background pic if ?>
</div>
<div class="show-header-info">
	<div class="main-container">
		<div class="show-cover-holder show-column">
			<img src=<?php echo $cover; ?> class="show-game-cover"/>
			<div class="game-side-info">
				<div>Genre: <span id="game-genres"></span></div>
				<div id="extra-side-info">
					<div>Developer: <span class="game-developer"></span></div>
					<div>Release Date: <span class="game-release-date"></span></div>
				</div>
			</div>
		</div>
		<div class="show-column">
			<!-- name, developer, release_date, in header -->
			<div class="info-in-header">
				<p id="show-game-name"><?php echo $game_name; ?></p>
				<p class="game-developer"></p>
				<p class="game-release-date"><?php echo $game->release_dates[0]->human; ?></p>
			</div>
			<!-- summary and storyline -->
			<div>
				<h3 class="header-tag">Summary</h3>
				<p class="game-summary-text">
				<?php
					 if(isset($game->summary)) {
					 	echo $game->summary;
					 } else {
					 	echo "<em>No summary was found for " . $game->name . "</em>";
					 }
				?></p>
				<?php if(isset($game->storyline)) { ?>
					<h3 class="header-tag">Storyline</h3>
					<p class="game-summary-text"><?php echo $game->storyline; ?></p>
				<?php } //end of storyline if ?>
			</div>
		</div>
	</div>	
</div>
<!-- video -->
<?php if(isset($game->videos)) { ?>
<div class="main-container" id="video-quick-nav">
	<h3 class="header-tag">Videos</h3>
</div>
<div id="video-background">
	<div id="home-banner-cover"></div>
	<div class="video-container">
		<div id="video-sidebar">
			<div class="vid-playlist-arrow playlist-top"><span class="flaticon-up-arrow slideshow-flaticon"></span></div>
			<div class="vid-playlist-arrow playlist-bottom"><span class="flaticon-down-arrow slideshow-flaticon"></span></div>
			<div id="video-playlist"></div>
		</div>
		<iframe id="yt-player" type="text/html" src=<?php echo "https://www.youtube.com/embed/" . $game->videos[0]->video_id . "?enablejsapi=1"; ?> frameborder=0></iframe>
	</div>
</div>
<?php } //end of video if ?>
<!-- screenshots -->
<?php if(isset($game->screenshots)) { ?>
<div class="main-container" id="screenshot-quick-nav">
	<h3 class="header-tag">Screenshots</h3>
</div>
<div id="screenshot-wrappers">
	<div class="flex-wrapper show-flex">
		<div class="flex-item flex-main flex-hover"></div>
		<div class="flex-item flex-small flex-hover"></div>
	</div>
	<div class="flex-wrapper show-flex">
		<div class="flex-item flex-small flex-hover"></div>
		<div class="flex-item flex-main flex-hover"></div>
	</div>
</div>
<?php } //end of screenshot if ?>
<?php } else {
	//if no game was found
	include "views/error_page.php";
}
?>

