//-------------------------\\
//-------------------------\\
// AJAX module
//-------------------------\\
//-------------------------\\
var ajax = function() {
	function fetchUpcomingGames() {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "ajax/home/get_upcoming_games.php", true);
		xhr.send();
		xhr.onreadystatechange = function() {
			if(xhr.readyState === 4) {
				if(xhr.status === 200) {
					//set upcomingGames with xhr data
					apiObjects.setUpcomingGames(JSON.parse(xhr.responseText));
					//add cards to dom
					gameRow.addCards(dom.id('upcoming-games'), "upcoming");
				} else {
					console.log("false");
				}
			}
		}
	}
	function searchQuery(q) {
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "ajax/home/search_query.php", true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send(q);
		xhr.onreadystatechange = function() {
			if(xhr.readyState === 4) {
				if(xhr.status === 200) {
					var resp = JSON.parse(xhr.responseText);
					//if no length an error obj has been returned
					if(typeof resp.length === 'undefined') {
						//run error function
						console.log(resp.message);
					} else {
						//set new JSON response in gameSearch module
						gameSearch.setGames(resp);
						//add games to search container
						gameSearch.gameCallback();
					}
				} else {
					var resp = JSON.parse(xhr.responseText)
					console.log(resp.message);
				}
			}
		}
	}
	return {
		fetchUpcomingGames: fetchUpcomingGames,
		searchQuery: searchQuery
	}
}();
//-------------------------\\
//-------------------------\\
// Search Module
//-------------------------\\
//-------------------------\\
var gameSearch = function() {
	var currentGames = [];
	var callTimeout = null;
	function search() {
		var input = dom.id('search-input');
		clearTimeout(callTimeout);
		dom.id('show-more').style.display = "none";
		if(input.value.length >= 3) {
			//settimeout so if user is still typing it won't make a bunch of calls
			callTimeout = setTimeout(makeCall, 300);
		}
	}
	function makeCall() {
		var input = dom.id('search-input');
		//style 'trending games' header
		styleTransitionHeader("Showing Results for '" + input.value + "'");
		//clear old results if any
		dom.id('search-results').innerHTML = "";
		//show loading gif
		loadingGif(true);
		//hide main home container, show search results
		searchTransition(true);
		//make call
		var postData = "q=" + input.value;
		ajax.searchQuery(postData);
	}
	function showMore() {
		//hide show more button
		dom.id('show-more').style.display = "none";
		var input = dom.id('search-input');
		//get amount found for offset
		var amount = dom.class('search-game-card').length + 4;
		var postData = "q=" + input.value + "&offset=" + amount;
		//show loader
		loadingGif(true);
		//make call
		ajax.searchQuery(postData);
	}
	function searchTransition(start) {
		//all changes made when search is started
		if(start) {
			//fade out main container
			dom.id('main-home-container').style.opacity = "0";
			setTimeout(function() { dom.id('main-home-container').style.display = "none"; }, 250);
			//fade in search container
			dom.id('search-home-container').style.display = "block";
			//fade in home-quick-nav
			dom.id('home-nav').style.opacity = "1";
		} else {
			//change first h2 back
			styleTransitionHeader("Trending Games");
			//fade out main container
			dom.id('main-home-container').style.display = "block"
			dom.id('main-home-container').style.opacity = "1";
			//fade in search container
			dom.id('search-home-container').style.display = "none";
			//fade in home-quick-nav
			dom.id('home-nav').style.opacity = "0";
			//clear current search input
			dom.id('search-input').value = "";
			//hide show more
			dom.id('show-more').style.display = "none";
		}
	}
	function gameCallback() {
		//hide loading gif
		loadingGif(false);
		//append results
		var resultsDiv = dom.id('search-results');
		if(currentGames.length > 0) {
			//iterate through currentGames, add game cards to dom
			for(var i=0;i<currentGames.length;i++) {
				if(currentGames[i].hasOwnProperty('cover') && currentGames[i].hasOwnProperty('release_dates')) {
					var gameCard = gameRow.gameCardHTML(currentGames[i]);
					//give card specific classes for search
					gameCard.className = "search-game-card animation-element";
					//append to results
					resultsDiv.appendChild(gameCard);
				}
			}
			//game cards have same class as news and will appear on scroll, manually activate to show them
			windowEvents.checkView();
			//if games returned is equal to limit on call, show 'show-more' else hide
			if(currentGames.length === 25) {
				dom.id('show-more').style.display = "block";
			} else dom.id('show-more').style.display = "none";
		} else {
			if(dom.class('search-game-card').length === 0) {
				resultsDiv.innerHTML = "<span style='font-size:3em;margin-top:30px'>No results found</span>";
			}
			dom.id('show-more').style.display = "none"
		}
	}
	function searchSubmit(e) {
		e.preventDefault();
		dom.id('search-input').blur();
		if(dom.id('search-input').value.length > 0) {
			makeCall();
		}
	}
	function setCurrentGames(gamesJSON) {
		currentGames = gamesJSON;
	}
	function loadingGif(show) {
		var loader = dom.id('search-loading');
		if(!show) {
			loader.style.display = "none";
		} else loader.style.display = "inline-block";
	}
	function styleTransitionHeader(text) {
		dom.id('in-transition-header').innerHTML = text;
	}
	function quickNavClick(e) {
		if(e.target.className === "flaticon-back" || e.target.getAttribute('id') === "back-to-menu") {
			//revert home page back to normal state
			searchTransition(false);
		} else {
			//scroll to top
			animateWinScroll(0, 250);
		}
	}
	return {
		search: search,
		submit: searchSubmit,
		setGames: setCurrentGames,
		gameCallback: gameCallback,
		showMore: showMore,
		quickNav: quickNavClick
	}
}();
//-------------------------\\
//-------------------------\\
// Game Row slideshow module
//-------------------------\\
//-------------------------\\
var gameRow = function() {
	var slideRunning = false;
	function slideRow() {
		//get api array associated with this game row
		if(this.parentNode.getAttribute('id') === "trending-games") {
			var objArr = apiObjects.trendingGames();
		} else {
			var objArr = apiObjects.upcomingGames();
		}
		//if animation is not running
		if(objArr && !slideRunning) {
			slideRunning = true;
			//determine direction of arrow
			if(/slide-right/i.test(this.className)) {
				var direction = "right";
			} else {
				var direction = "left";
			}
			//determine amount of objects to add to div
			var cardWidth = dom.class('game-card')[0].clientWidth;
			var rowWidth = dom.class('home-game-row')[0].clientWidth;
			var amount = Math.floor(rowWidth/cardWidth);
			//create new element to hold game cards
			var newHolder = document.createElement("div");
			newHolder.className = "game-card-holder";
			//find game card holder
			var cardHolder = false;
			for(var i=0;i<this.parentNode.children.length;i++) {
				if(this.parentNode.children[i].className === "game-card-holder") {
					cardHolder = this.parentNode.children[i];
					break;
				}
			}
			//latestCard holds the first or last game card in the holder
			if(direction === "right") {
				var latestCard = cardHolder.children[cardHolder.children.length - 1];
				//assign which direction newHolder should be coming from
				newHolder.style.left = cardHolder.clientWidth + "px";
			} else {
				var latestCard = cardHolder.children[0];
				newHolder.style.left = -Math.abs(cardHolder.clientWidth) + "px";
			}
			//find index of latestCard in objArr by comparing game id in card html with id's in objArr
			var latestIndex = 0;
			for(var j=0;j<objArr.length;j++) {
				if(latestCard.children[0].innerHTML === String(objArr[j].id)) {
					latestIndex = j;
					break;
				} 
			}
			//find objects in objArr that are next in line
			for(var k=0;k<amount;k++) {
				if(direction === "left") {
					latestIndex -= 1;
					if(latestIndex >= 0) {
						//insert before so game cards are added in the same left to right order as they are in objArr
						newHolder.insertBefore(gameCardHTML(objArr[latestIndex]), newHolder.firstChild);
					} else {
						newHolder.insertBefore(gameCardHTML(objArr[objArr.length - 1]), newHolder.firstChild);
						latestIndex = objArr.length - 1;
					}
				} else {
					latestIndex += 1;
					if(latestIndex <= objArr.length - 1) {
						newHolder.appendChild(gameCardHTML(objArr[latestIndex]));
					} else {
						newHolder.appendChild(gameCardHTML(objArr[0]));
						//change latestIndex
						latestIndex = 0;
					}
				}
			}
			//append newHolder to game row
			this.parentNode.appendChild(newHolder);
			//then do animation
			var cardHolderLeft = cardHolder.clientWidth + "px";
			if(direction === "right") { cardHolderLeft = -Math.abs(cardHolder.clientWidth) + "px"; }
			//animate current cardHolder 
			cardHolder.style.left = cardHolderLeft;
			//animate newHolder into cardHolder's place
			newHolder.style.left = "0px";
			//remove cardHolder and set slideRunning as false after css animation
			setTimeout(function() { slideRunning = false;cardHolder.remove() }, 300)
		}
	}
	function addInitialCards(row, arrType) {
		//determine which array from apiObjects to use
		if(arrType === "trending") {
			var objArr = apiObjects.trendingGames();
		} else {
			var objArr = apiObjects.upcomingGames();
		}
		//find game card holder
		for(var i=0;i<row.children.length;i++) {
			if(/game-card-holder/.test(row.children[i].className)) {
				var holderRow = row.children[i];
				break;
			}
		}
		//clear HTML in holderRow, to clear loading gif
		holderRow.innerHTML = "";
		//get games from apiObjects
		if(objArr) {
			//create doc fragment, put 4 game card divs into it then append to this row
			var newCards = document.createDocumentFragment();
			for(var i=0;i<4;i++) {
				newCards.appendChild(gameCardHTML(objArr[i]));
			}
			holderRow.appendChild(newCards);
		}
	}
	function gameCardHTML(object) {
		//deal with esrb rating
		var esrb = "assets/images/rp-rating.png";
		if(object.hasOwnProperty('esrb')) { esrb = apiInfo.esrb(object.esrb.rating); }
		//check if no image
		if(object.hasOwnProperty('cover')) {
			var cover = convertPicUrl(object.cover.url);
		} else var cover = "assets/images/chest.jpg";
		//get genres
		var subHeader = "";
		if(object.hasOwnProperty('genres')) { 
			subHeader = getGameGenres(object.genres); 
		} else if(object.hasOwnProperty('release_dates')) {
			subHeader = object.release_dates[object.release_dates.length - 1].human;
		}
		var insideCard = '<div class="game-id">' + object.id + '</div>' +
							'<a href="/' + object.id + '/' + object.slug + '" class="game-card-hover"><span class="game-card-hover-title">' + object.name + '</span></a>' +
							'<div class="home-card-overlay"></div>' +
							'<div class="home-card-info game-card-info">' +
								'<p class="game-card-title">' + object.name + '</p>' +
								'<p class="game-card-genres">' + subHeader + '</p>' +
								'<img src="' + esrb + '" class="game-card-rating"/>' +
							'</div>' +
							'<div class="card-pic-holder">' +
								'<img src="' + cover + '" class="home-card-pic"/>' +
							'</div>';
		var card = document.createElement("div");
		card.className = "game-card";
		card.innerHTML = insideCard;
		return card;
	}
	function getGameGenres(genres) {
		var genreArr = [];
		//iterate through genres pass to apiInfo to decipher id
		if(genres.length >= 2) {
			for(var i=0;i<2;i++) {
				genreArr.push(apiInfo.genre(genres[i]));
			}
		} else {
			genreArr.push(apiInfo.genre(genres[0]));
		}
		//return joined arr
		if(genreArr.length > 1) {
			return genreArr.join("/")
		} else return genreArr[0];
	}
	function convertPicUrl(url) {
		var splitUrl = url.split("/");
		return "//images.igdb.com/igdb/image/upload/t_cover_big/" + splitUrl[splitUrl.length - 1];
	}
	return {
		slide: slideRow,
		addCards: addInitialCards,
		gameCardHTML: gameCardHTML
	}
}();
//-------------------------\\
//-------------------------\\
// News/Pulses Module
//-------------------------\\
//-------------------------\\
var pulseFlex = function() {
	function addNewsOnLoad() {
		dom.id('news-loader').style.display = "none";
		var pulses = apiObjects.pulses();
		var wrappers = dom.class('flex-wrapper');
		if(pulses) {
			//iterate through all flex wrappers in doc
			for(var i=0;i<wrappers.length;i++) {
				//iterate through each individual flex item in wrappers[i]
				for(var j=0;j<wrappers[i].children.length;j++) {
					//iterate through objects in pulses
					for(var k=0;k<pulses.length;k++) {
						if(!pulses[k].hasOwnProperty('added')) {
							if(pulses[k].hasOwnProperty('image')) {
								//add innerHTML with this object to current item
								wrappers[i].children[j].innerHTML = pulseHTML(pulses[k], pulses[k].image);
								//update pulse object with added prop
								pulses[k].added = true;
								break;
							} else if(k > pulses.length - 2) {
								//add obj anyway with a default pic if almost to end of pulses arr
								wrappers[i].children[j].innerHTML = pulseHTML(pulses[k], false);
								//update pulse object with added prop
								pulses[k].added = true;
								break;
							}
						}
					}
					//randomly change order flex items appear in their respective rows
					if(Math.random() >= 0.5) {
						wrappers[i].children[j].style.order = "2";
					}
				}
			}
		}
	}
	function pulseHTML(object, img) {
		if(!img) { img = "assets/images/chest.jpg"; }
		return 	'<div class="news-site-info">' + apiInfo.site(object.category) + '</div>' +
				'<a class="home-card-overlay" target="_blank" href="' + object.url + '"></a>' +
				'<div class="home-card-info news-card-info">' +
					'<a href="' + object.url + '" class="news-headline">' + object.title + '</a>' +
				'</div>' +
				'<div class="card-pic-holder">' +
					'<img src="' + img + '" class="home-card-pic"/>' +
				'</div>';

	}
	return {
		addNewsOnLoad: addNewsOnLoad
	}
}();
//-------------------------\\
//-------------------------\\
// CSS Scroll Animations Module
//-------------------------\\
//-------------------------\\
var windowEvents = function() {
	var animationElements = dom.class('animation-element');
	var bWindow = window;
	var lastWindowScrollPos = 0;
	//check if any elements with animation-element class are in view, animates if so
	function checkIfInView() {
		var winHeight = bWindow.innerHeight;
		var winScroll = bWindow.scrollY;
		var winBottom = (winHeight + winScroll);
		for(var i=0;i<animationElements.length;i++) {
			var thisElement = animationElements[i];
			var elHeight = thisElement.offsetHeight;
			var elScroll = thisElement.offsetTop;
			var elBottom = (elHeight + elScroll);
			if(elScroll <= winBottom) {
				if(!/in-view/ig.test(thisElement.className)) {
					thisElement.className = thisElement.className + " in-view";
				}
			} /*else {
				var oldClasses = thisElement.className.replace(/in-view/gi, '');
				thisElement.className = oldClasses;
			} */
		}
	}
	function adjustBanner() {
		var searchLogo = dom.id('home-banner-search');
		var banner = dom.id('home-page-banner');
		var transition = dom.id('home-banner-transition');
		var bannerArea = banner.offsetHeight - transition.offsetHeight;
		var logoArea = bannerArea - searchLogo.offsetHeight;
		searchLogo.style.marginTop = parseInt(logoArea)/2 + "px";
	}
	return {
		checkView: checkIfInView,
		adjustBanner: adjustBanner
	}
}();
//-------------------------\\
//-------------------------\\
// DOMContentLoaded Events
//-------------------------\\
//-------------------------\\
window.addEventListener('DOMContentLoaded', function() {
	//trending games holder
	gameRow.addCards(dom.id('trending-games'), "trending");
	//add news pulses
	pulseFlex.addNewsOnLoad();
	/* page load ajax calls */
	ajax.fetchUpcomingGames();
	//windowEvents.adjustBanner();
});
//-------------------------\\
//-------------------------\\
// Events
//-------------------------\\
//-------------------------\\
window.addEventListener('load', function() {
	//game row slide clicks
	(function slideShowClick() {
		var icons = dom.class('slideshow-icon-div');
		for(var i=0;i<icons.length;i++) {
			icons[i].addEventListener('click', gameRow.slide, true);
		}
	})();
	/* window listeners */
	windowEvents.checkView();
	window.addEventListener('scroll', function() {
		windowEvents.checkView();
	});
	//windowEvents.adjustBanner();
	window.addEventListener('resize', function() {
		windowEvents.checkView();
		//windowEvents.adjustBanner();
	});
	//search bar listener
	dom.id('search-input').addEventListener('keyup', gameSearch.search);
	//search form listener
	dom.id('search-form').addEventListener('submit', gameSearch.submit);
	//load more button
	dom.id('show-more').addEventListener('click', gameSearch.showMore);
	//listen for search quick nav click
	dom.id('home-nav').addEventListener('click', gameSearch.quickNav);
	//saerch icon click, acts same as form submit
	dom.id('search-icon').addEventListener('click', gameSearch.submit);
});