//-------------------------\\
//-------------------------\\
// Game Info 
//-------------------------\\
//-------------------------\\
var gameInfo = function() {
	function pageLoadInfo() {
		var game = gameObj.game();
		//get genre(s) from ids in gameObj
		var genres = document.createDocumentFragment();
		for(var i=0;i<game.genres.length;i++) {
			genres.appendChild(gameGenreHTML(game.genres[i]));
		}
		dom.id('game-genres').appendChild(genres);
		//add developer
		var developer = gameObj.company();
		if(!developer || developer === "false") { developer = "Unknown"; }
		fillInInfo(dom.class('game-developer'), developer);
		//add release date
		fillInInfo(dom.class('game-release-date'), game.release_dates[0].human);
		function fillInInfo(classArr, innerText) {
			for(var i=0;i<classArr.length;i++) {
				classArr[i].innerHTML = innerText;
			}
		}
		function gameGenreHTML(genreId) {
			var genre = apiInfo.genre(genreId);
			var newSpan = document.createElement('span');
			newSpan.className = "game-genre";
			newSpan.innerHTML = genre;
			return newSpan;
		}
	}
	return {
		pageLoadInfo: pageLoadInfo
	}
}();
//-------------------------\\
//-------------------------\\
// YouTube API iFrame
//-------------------------\\
//-------------------------\\
var showPlayer = function() {
	var player;
	var playlistScrollTop = 0;
	function loadAPI() {
		if(gameObj.game().hasOwnProperty('videos')) {
			//set up other videos in playlist
			setVideoPlaylist();
			//loads in youtube API script
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScript = dom.tag('script')[0];
			firstScript.parentNode.insertBefore(tag, firstScript);
			//initializes player when script is loaded
			function onYouTubeIframeAPIReady() {
				player = new YT.Player('yt-player', {
					events: {
						'onReady': onPlayerReady,
						'onStateChange': onPlayerStateChange
					}
				});
			}
		}
	}
	function setVideoPlaylist() {
		//arrange all videos attached to game in playlist besides yt player
		var game = gameObj.game();
		if(game && game.hasOwnProperty('videos')) {
			var videos = game.videos;
			var playlist = document.createDocumentFragment();
			for(var i=0;i<videos.length;i++) {
				playlist.appendChild(videoDivHTML(videos[i]));
			}
			dom.id('video-playlist').appendChild(playlist);
		}
		function videoDivHTML(object) {
			var div = document.createElement('div');
			div.className = "video-div";
			var vidHTML = '<div class="home-card-info">' +
							'<p class="vid-div-text">' + object.name + '</p>' +
						  '</div>' +
						  '<div class="home-card-overlay"></div>' +
						  '<div class="card-pic-holder">' +
						  	'<img src="' + getYoutubeThumbnail(object.video_id) + '" class="home-card-pic">' +
						  '</div>';
			div.innerHTML = vidHTML;
			return div;
		}
		//find first video div and highlight text to show playing video
		dom.class('video-div')[0].style.color = "#ceba7e";
	}
	function scrollPlaylist() {
		//determine direction of arrow
		if(/playlist-top/i.test(this.className)) {
			var direction = "up";
		} else {
			var direction = "down";
		}
		//get all playlist props
		var playlist = dom.id('video-playlist');
		var playlistHeight = playlist.clientHeight;
		var playlistScrollHeight = playlist.scrollHeight;
		if(playlist.style.top) { playlistScrollTop = parseInt(playlist.style.top); }
		//max top is the maximum negative top the playlist can be 
		var maxTop = playlistScrollHeight - playlistHeight;
		var newTop = Math.floor(playlistHeight * .4);
		//if down top attr is going to be negative, if its greater than maxTop it just goes to the bottom
		if(direction === "down") {
			if((newTop + Math.abs(playlistScrollTop)) > maxTop) {
				newTop = -Math.abs(maxTop + 50);
			} else newTop = -Math.abs(newTop);
		} else {
			if(newTop + Math.abs(playlistScrollTop) > 0) {
				newTop = 0;
			} else newTop = Math.abs(newTop);
		}
		//apply new top to playlist, css deals with animation
		playlist.style.top = newTop + "px";
	}
	function changeVideo(e) {
		resetActiveVidText();
		var vidDiv = e.target.parentNode;
		//if clicked on text
		if(vidDiv.className !== "video-div") { vidDiv = vidDiv.parentNode }
		//add active gold color to this vidDiv
		vidDiv.style.color = "#ceba7e";
		//find pic src (has same id as video)
		var picSrc = null;
		for(var i=0;i<vidDiv.children.length;i++) {
			var childClassName = vidDiv.children[i].className;
			if(childClassName === "card-pic-holder") {
				picSrc = vidDiv.children[i].children[0].getAttribute('src');
				break;
			}
		}
		//parse video id from preview pic url
		var picSrcArr = picSrc.split("/"), vidId = picSrcArr[picSrcArr.length - 2];
		//change iFrame source
		//dom.id('yt-player').setAttribute('src', "https://www.youtube.com/embed/" + vidId + "?enablejsapi=1");
		dom.id('yt-player').contentWindow.location.replace("https://www.youtube.com/embed/" + vidId + "?enablejsapi=1")
		//resets gold active video text
		function resetActiveVidText() {
			var vidDivs = dom.class('video-div');
			for(var i=0;i<vidDivs.length;i++) {
				vidDivs[i].style.color = "white";
			}
		}
	}
	function getYoutubeThumbnail(vidId) {
		return 'https://img.youtube.com/vi/' + vidId + "/0.jpg";
	}
	function onPlayerReady(e) {

	}
	function onPlayerStateChange(e) {
	}
	return {
		start: loadAPI,
		scrollPlaylist: scrollPlaylist,
		changeVideo: changeVideo
	}
}();
//-------------------------\\
//-------------------------\\
// Screenshot Flex Module
//-------------------------\\
//-------------------------\\
var gameImages = function() {
	function addImages() {
		var game = gameObj.game();
		if(game && game.hasOwnProperty('screenshots')) {
			//shuffle screenshots
			var images = shuffleImages(shuffleImages(game.screenshots));
			//cycle through flex wrappers (rows) and their children
			var wrappers = dom.class('flex-wrapper');
			if(game.screenshots.length > 1) {
				//iterate through all flex wrappers in doc
				for(var i=0;i<wrappers.length;i++) {
					//iterate through each individual flex item in wrappers[i]
					for(var j=0;j<wrappers[i].children.length;j++) {
						//iterate through objects in images
						for(var k=0;k<images.length;k++) {
							if(!images[k].hasOwnProperty('added')) {
								if(images[k].hasOwnProperty('url')) {
									var pic = picHTML(images[k]);
									//add innerHTML with this object to current item
									wrappers[i].children[j].innerHTML = pic;
									//update image object with added prop
									images[k].added = true;
									//show wrapper
									wrappers[i].style.display = "flex";
									break;
								}
							}
						}
						//randomly change order flex items appear in their respective rows
						if(Math.random() >= 0.5) {
							wrappers[i].children[j].style.order = "2";
						}
					}
				}
				//if any extra pics add container
				mainWrapper();
			} else {
				//if only one pic, make main container
				mainWrapper();
			}
		}
		function mainWrapper() {
			//randomly add in another flex wrapper with show-flex-main
			var newWrapper = document.createElement('div');
			newWrapper.className = "flex-wrapper show-flex-main flex-hover";
			for(var i=0;i<images.length;i++) {
				if(!images[i].hasOwnProperty('added')) {
					var pic = picHTML(images[i]);
					newWrapper.innerHTML = '<div class="flex-item">' +
												pic +
											'</div>';
					//randomly append 'main' new wrapper to screenshot wrapper container
					dom.id('screenshot-wrappers').insertBefore(newWrapper, wrappers[Math.floor(Math.random() * wrappers.length)]);						
					break;
				}
			}
		}
		function picHTML(image) {
			var imgUrl = convertPicUrl(image.url);
			var picHTML = '<a href="' + imgUrl + '" target="_blank" class="card-pic-holder">' +
								'<span class="flaticon-binoculars show-image-icon"></span>' +
								'<img src="' + imgUrl + '" class="home-card-pic"/>' +
							'</a>';
			return picHTML;
		}
	}
	function shuffleImages(arr) {
		for(var i=0;i<arr.length;i++) {
			var rand = Math.floor(Math.random() * (i + 1));
			var tempArr = arr[i];
			arr[i] = arr[rand];
			arr[rand] = tempArr;
		}
		return arr;
	}
	function convertPicUrl(url) {
		var splitUrl = url.split("/");
		return "//images.igdb.com/igdb/image/upload/t_screenshot_big/" + splitUrl[splitUrl.length - 1];
	}
	return {
		addImages: addImages
	}
}();
//-------------------------\\
//-------------------------\\
// CSS Scroll Animations Module
//-------------------------\\
//-------------------------\\
var navScroll = function() {
	var bWindow = window;
	var currentQuickNavElement = null;
	function checkNavScroll() {
		var mainNav = dom.id('main-navbar');
		var quickNav = dom.id('quick-navbar')
		var header = dom.id('show-header');
		var headerHeight = header.clientHeight;
		var winScroll = bWindow.scrollY;
		if(winScroll > headerHeight) {
			if(mainNav.style.top !== "0px") {
				mainNav.style.top = "0px";
			}
		} else {
			if(mainNav.style.top === "0px") {
				mainNav.style.top = "-55px";
			}
		}
	}
	function quickNavHighlight() {
		var headerContainer = dom.class("show-header-info")[0];
		var videoContainer = dom.id('video-quick-nav');
		var imageContainer = dom.id('screenshot-quick-nav');
		var winScroll = bWindow.scrollY;
		if(winScroll >= imageContainer.offsetTop - 65) {
			var span = dom.id('nav-screenshots');
		} else if(winScroll >= videoContainer.offsetTop - 65) {
			var span = dom.id('nav-video');
		} else {
			var span = dom.id('nav-info');
		}
		if(!currentQuickNavElement || currentQuickNavElement.getAttribute('id') !== span.getAttribute('id')) {
			resetNavElements();
			currentQuickNavElement = span;
			span.style.color = "white";
			span.style.borderBottom = "2px solid white";
		}
		function resetNavElements() {
			var navEls = dom.class('quick-nav-span');
			for(var i=0;i<navEls.length;i++) {
				navEls[i].style.color = "rgba(255, 255, 255, 0.5)";
				navEls[i].style.borderBottom = "none";
			}
		}
	}
	function quickNavClick(e) {
		var scroll = 0;
		if(e.target.getAttribute('id') === "nav-info") {
			scroll = 0;
		} else if(e.target.getAttribute('id') === "nav-video") {
			scroll = dom.id('video-quick-nav').offsetTop;
		} else {
			scroll = dom.id('screenshot-quick-nav').offsetTop;
		}
		animateWinScroll(scroll - 10, 250);
	}
	return {
		checkNav: checkNavScroll,
		quickNav: quickNavHighlight,
		quickNavClick: quickNavClick
	}
}();
//-------------------------\\
//-------------------------\\
// DOMContentLoaded Events
//-------------------------\\
//-------------------------\\
window.addEventListener('DOMContentLoaded', function() {
	var game = gameObj.game();
	gameImages.addImages();
	showPlayer.start();
	gameInfo.pageLoadInfo();
	//video playlist arrow click
	(function() {
		var arrows = dom.class('vid-playlist-arrow');
		for(var i=0;i<arrows.length;i++) {
			arrows[i].addEventListener('click', showPlayer.scrollPlaylist);
		}
	})();
	//listen for clicks to change video
	if(game.hasOwnProperty('videos')) { dom.id('video-playlist').addEventListener('click', showPlayer.changeVideo); }
	//listen to scrolls for navbar
	if(game.hasOwnProperty('screenshots') && game.hasOwnProperty('videos')) {
		navScroll.checkNav();
		navScroll.quickNav();
		window.addEventListener('scroll', function() {
			navScroll.checkNav();
			navScroll.quickNav();
		});
		//listen for quick nav clicks
		dom.id('quick-navbar').addEventListener('click', navScroll.quickNavClick);
	}
});