//-------------------------\\
//-------------------------\\
// Dom Manipulation Module
//-------------------------\\
//-------------------------\\
var dom = function() {
	function getId(element) {
		return document.getElementById(element);
	}
	function getClass(element) {
		return document.getElementsByClassName(element);
	}
	function query(element) {
		return document.querySelector(element);
	}
	function queryAll(element) {
		return document.querySelectorAll(element);
	}
	function tagName(element) {
		return document.getElementsByTagName(element);
	}
	return {
		id: getId,
		class: getClass,
		query: query,
		queryAll: queryAll,
		tag: tagName
	}
}();
function animateWinScroll(endPos, duration) {
 	//holds setInterval
  	var animation = null;
  	//get where window is now
  	var winY = window.scrollY;
  	//amount of intervals setinterval will run
  	var intervals = duration/5;
  	//diff between starting and end point
 	var baselineChange = winY - endPos;
 	//increment for each interval
 	var increment = baselineChange/intervals
 	//loops holds current interval num
  	var loops = 0;
  	animation = setInterval(function() {
    	if(loops <= intervals) {
      		var currentY = window.scrollY;
      		window.scrollTo(0, currentY - increment);
    	} else {
      		clearInterval(animation);
    	}
    	loops++;
  	}, 5);
}
//-------------------------\\
//-------------------------\\
// IGDB API reference module
//-------------------------\\
//-------------------------\\
var apiInfo = function() {
	function esrbRating(rating) {
		switch(rating) {
			case 2:
				return "assets/images/c-rating.png";
				break;
			case 3:
				return "assets/images/e-rating.png";
				break;
			case 4:
				return "assets/images/e10-rating.png";
				break;
			case 5:
				return "assets/images/t-rating.png";
				break;
			case 6:
				return "assets/images/m-rating.png";
				break;
			case 7:
				return "assets/images/ao-rating.png";
				break;
			default:
				return "assets/images/rp-rating.png";
				break;
		}
	}
	function pulseWebsite(site) {
		switch(site) {
			case 1:
				return "Reddit";
				break;
			case 2:
				return "Kickstarter";
				break;
			case 3:
				return "Kotaku";
				break;
			case 4:
				return "Polygon";
				break;
			case 5:
				return "KillScreen";
				break;
			case 6:
				return "GameInformer";
				break;
			case 7:
				return "Rock, Paper, Shotgun";
				break;
			case 8:
				return "Wired Game/Life";
				break;
			case 9:
				return "N4G";
				break;
			case 10:
				return "Escapist";
				break;
			case 11:
				return "PCGamer";
				break;
			case 12:
				return "IGN";
				break;
			case 13:
				return "Indie Games";
				break;
			case 14:
				return "Destructoid";
				break;
			case 15:
				return "Niche Gamer";
				break;
		}
	}
	function getPlatform(id) {
		switch(id) {
			case 130:
				return "Nintendo Switch";
				break;
			case 74:
				return "Windows Phone";
				break;
			case 28:
				return "Mobile";
				break;
			case 49:
				return "Xbox One";
				break;
			case 48:
				return "PlayStation 4";
				break;
			case 47:
				return "Virtual Console (Nintendo)";
				break;
			case 46:
				return "PlayStation Vita";
				break;
			case 45:
				return "PlayStation Network";
				break;
			case 41:
				return "Wii U";
				break;
			case 39:
				return "iOS";
				break;
			case 38:
				return "PSP";
				break;
			case 37:
				return "Nintendo 3DS";
				break;
			case 36:
				return "Xbox Live Arcade";
				break;
			case 34:
				return "Android";
				break;
			case 33:
				return "Game Boy";
				break;
			case 32:
				return "Sega Saturn";
				break;
			case 29:
				return "Sega Genesis";
				break;
			case 24:
				return "Game Boy Advance";
				break;
			case 23:
				return "Dreamcast";
				break;
			case 22:
				return "Game Boy Color";
				break;
			case 21:
				return "Nintendo Gamecube";
				break;
			case 19:
				return "SNES";
				break;
			case 18:
				return "NES";
				break;
			case 15:
				return "Commodore 64";
				break;
			case 14:
				return "Mac";
				break;
			case 13:
				return "PC DOS";
				break;
			case 12:
				return "Xbox 360";
				break;
			case 11:
				return "Original Xbox";
				break;
			case 9:
				return "PlayStation 3";
				break;
			case 8:
				return "PlayStation2";
				break;
			case 7:
				return "PlayStation";
				break;
			case 6:
				return "PC";
				break;
			case 5:
				return "Wii";
				break;
			case 4:
				return "Nintendo 64";
				break;
			case 3:
				return "Linux";
				break;
			case 20:
				return "Nintendo DS";
				break;
		}
	}
	function getGenre(id) {
		switch(id) {
			case 33:
				return "Arcade";
				break;
			case 32:
				return "Indie";
				break;
			case 31:
				return "Adventure";
				break;
			case 30:
				return "Pinball";
				break;
			case 26:
				return "Quiz/Trivia";
				break;
			case 25:
				return "Hack n' Slash/Beat em' Up";
				break;
			case 24:
				return "Tactical";
				break;
			case 16:
				return "Turn Based Strategy";
				break;
			case 15:
				return "Strategy";
				break;
			case 14:
				return "Sport";
				break;
			case 13:
				return "Simulator";
				break;
			case 12:
				return "RPG";
				break;
			case 11:
				return "RTS";
				break;
			case 10:
				return "Racing";
				break;
			case 9:
				return "Puzzle";
				break;
			case 8:
				return "Platformer";
				break;
			case 7:
				return "Music";
				break;
			case 5:
				return "Shooter";
				break;
			case 4:
				return "Fighting";
				break;
			case 2:
				return "Point-and-Click";
				break;
		}
	}
	function getStatus(id) {
		switch(id) {
			case 0:
				return "Released";
				break;
			case 2:
				return "Alpha";
				break;
			case 3:
				return "Beta";
				break;
			case 4:
				return "Early Access";
				break;
			case 5:
				return "Offline";
				break;
			case 6:
				return "Cancelled";
				break;
		}
	}
	return {
		esrb: esrbRating,
		site: pulseWebsite,
		platform: getPlatform,
		genre: getGenre,
		status: getStatus
	}
}();