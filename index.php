<!DOCTYPE html>
<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once "config.php";
require_once "classes/all_classes.php";
?>
<html>
<head>
	<?php
	$host = substr($_SERVER['HTTP_HOST'], 0, 5);
	if(in_array($host, array('local', '127.0', '192.1'))) {
		echo '<base href="/game_db/"/>';
	} else {
		echo '<base href="/"/>';
	}
	include "head.php"; 
	?>
</head>
<body>
<div id="main-background">
<?php
isset($_GET['p']) ? $p = $_GET['p'] : $p = "";
switch($p) {
	case "game":
		include "views/games/show.php";
		break;
	case "error":
		include "views/error_page.php";
		break;
	default:
		include "views/home.php";
		break;
}
?>
<div id="footer">
	<div id="footer-links">
		<a href="/"><span class="flaticon-war"></span>About</a>
		<a href="http://www.connorhandy.com"><span class="flaticon-targeting"></span>Contact</a>
		<a href="/"><span class="flaticon-binoculars"></span>News</a>
		<a href="/"><span class="flaticon-map"></span>Blog</a>
	</div>
	<div>
		<img src="assets/images/footer-pic.png" id="footer-pic">
	</div>
	<p>&copy; Connor Handy, 2017</p>
</div>
</div>
</body>
</html>