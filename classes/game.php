<?php
Class Game {
	
	function searchGames($q, $offset) {
		$url = self::requestSearchString("games", "name,release_dates,esrb,cover,slug", $q, 25, $offset);
		$request = self::sendRequest($url);
		return $request;
	}

	function getGame($id) {
		$url = self::requestIdString(["name", "summary", "storyline", "developers", "genres", "screenshots", "videos", "cover", "esrb", "release_dates", "storyline", "hypes", "popularity", "status"], $id);
		$request = self::sendRequest($url);
		return $request;
	}

	function getGameCompany($id) {
		$url = "https://igdbcom-internet-game-database-v1.p.mashape.com/companies/$id?fields=name";
		$request = self::sendRequest($url);
		return $request;
	}

	function requestSearchString($category, $fields, $search, $limit = 20, $offset = 0) {
		return "https://igdbcom-internet-game-database-v1.p.mashape.com/$category/?fields=$fields&filter[game][not_exists]&limit=$limit&offset=$offset&order=release_dates.date:desc&search=$search";
	}

	function requestIdString($fields, $id) {
		$split_fields = implode(",", $fields);
		return "https://igdbcom-internet-game-database-v1.p.mashape.com/games/$id?fields=$split_fields";
	}

	function upcomingGames() {
		$time = date('Y-m-d');
		$url = "https://igdbcom-internet-game-database-v1.p.mashape.com/games/?fields=name,release_dates,esrb,cover,slug&filter[release_dates.date][gte]=$time&filter[game][not_exists]&filter[cover][exists]&order=release_dates.date:asc:max&limit=16";
		$request = self::sendRequest($url);
		return $request;
	}

	function topTrendingGames($order) {
		$url = "https://igdbcom-internet-game-database-v1.p.mashape.com/games/?fields=name,hypes,genres,esrb,cover,slug&filter[game][not_exists]&limit=16&order=$order:desc:max";
		$request = self::sendRequest($url);
		return $request;
	}

	function getPulses() {
		$url = "https://igdbcom-internet-game-database-v1.p.mashape.com/pulses/?fields=category,title,url,image&order=published_at:desc&limit=15";
		$request = self::sendRequest($url);
		return $request;
	}

	function sendRequest($string) {
		$response = Unirest\Request::get($string,
		  array(
		    "X-Mashape-Key" => "TFrAnsp0uTmshUv87nQNYSzgh3ypp1FxqBdjsnJdTER9ePNzS6",
		    "Accept" => "application/json"
		  )
		);
		return $response;
	}
}
?>